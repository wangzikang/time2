\section{Introduction}

A knowledge graph is a large-scale knowledge base which encodes relational
knowledge of entities into a graph structure.
In knowledge graphs, a fact is typically stored as a triple $(h,r,t)$, 
which indicates that entity $h$ and entity $t$ have relation $r$.
There are many famous knowledge graphs such as WordNet \cite{wordnet}, Freebase \cite{freebase}, YAGO \cite{yago}, and Wikidata \cite{wikidata}. 
They have been widely adopted in various applications,
such as search, recommendation, question answering, to name a few.

A fundamental challenge in researches and applications related to knowledge graphs is representation learning.
It aims to learn low-dimensional continuous embeddings for the stored entities and relations with semantic knowledge encoded.
Various methods have been proposed to achieve this goal,
such as TransE \cite{TransE}, TransH \cite{TransH} and HolE \cite{HolE}.

It is noticeable that most existing methods completely ignore available temporal information in both learning and inference phases,
which means facts must be time-unknown \cite{tTransE}, i.e., triple $(h, r, t)$ cannot include temporal information.
However, many facts are valid only in a specific period \cite{HyTE}.
For example, the fact ``Andre Agassi is a professional tennis player''
is formalized as (Andre Agassi, is, professional tennis player) in a knowledge graph,
but it only holds from 1986 to 2006
as he started playing tennis as a professional player in 1986 and retired in 2006.

It is evident that temporal information can help learn more credible embeddings for 
both entities and relations.
The literature primarily proposed two kinds of approaches,
one of them preserves temporal order in the learned embeddings like t-TransE \cite{tTransE},
and the other encodes temporal information in the resulting embeddings explicitly.
HyTE \cite{HyTE} and TA-TransE \cite{timeSequence} are two
algorithms in this direction. 
HyTE projects entities and relations onto the same time-dependent
hyperplane, then applies the TransE condition \cite{TransE} to the projected embedding vectors.
TA-TransE first converts temporal information into temporal tokens (i.e.,
words), then learns the semantics of these tokens using recurrent neural networks.

Nevertheless, the effect of temporal information on the embeddings of both entities
and relations is still not well understood.
This paper devises models to investigate this question and learns high-quality
representations. 
We incorporate starting and ending times in the fact triple $(h, r, t)$ to encode the temporal information explicitly.
Thus, a fact is then denoted as $(h,r,t,\tau_s,\tau_e)$, 
which indicates entities $h$ and $t$ have relation $r$ from $\tau_s$ to $\tau_e$.
To explore how time affects embeddings,
we make three assumptions about whether embeddings of entity or relation are time-sensitive,
and build three different models corresponding to them.
For all three models,
we build two separate embedding spaces for relations and entities to
increase the expressive power of the models
and project embedding vectors
between these spaces according to the specific model assumption.
It is different from HyTE,
which uses only one space and projects embedding vectors onto hyperplanes.

We evaluate all the models on three tasks:
entity prediction, relation prediction, and temporal scope prediction.
The proposed \rel\ model achieves the state-of-the-art results on all tasks.
We further analyze the results and find that relations are more important than entities
in temporal knowledge learning. This observation can also be adapted to simplify
other algorithms, such as HyTE.
We also conduct experiments to show that embedding
transformations between spaces other than hyperplanes can enhance model performance.

Our contributions are listed as follows:
 \begin{itemize}
     \item We explore how temporal information affects the embeddings of entities and relations,
     which is an essential problem unexplored.
     We find that only relation embeddings are time-sensitive for translation-based models,
     the influence of time on entity embeddings can be ignored.
     \item We propose three new models to learn time-aware representations,
	among which model \rel{} is simple and effective, getting state-of-the-art results on three evaluation tasks.
     \item We show that existing models (e.g., HyTE) can be simplified based on our findings by only considering relations as time-sensitive and get equally good results.
 \end{itemize}
